import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        System.out.println("Calculator");
        System.out.println("0. Exit");
        System.out.println("1. Add");
        System.out.println("2. Subtract");
        System.out.println("3. Multiply");
        System.out.println("4. Divide");

        System.out.print("Selection: ");
        Scanner input = new Scanner(System.in);

        try {
            int userIn = input.nextInt();

            if (userIn == 0) {
                System.out.println("Exiting...");
                System.exit(0);
            }

            System.out.print("First number: ");
            int num1 = input.nextInt();

            System.out.print("Second number: ");
            int num2 = input.nextInt();


            switch (userIn) {
                case 1 ->  {
                    int sum = add(num1, num2);
                    System.out.printf("Your sum is: %2d%n", sum);
                }
                case 2 -> {
                    int diff = subtract(num1, num2);
                    System.out.printf("Your difference is: %2d%n", diff);
                }
                case 3 -> {
                    int product = multiply(num1, num2);
                    System.out.printf("Your product is: %2d%n", product);
                }
                case 4 -> {
                    int quotient = divide(num1, num2);
                    System.out.printf("Your quotient is: %2d%n", quotient);
                }
                default -> {
                    System.out.println("Invalid option!");
                }
            }
        }
        catch(Exception e) {
            System.out.println("Invalid input detected");
        }
    }


    static int add(int num1, int num2) {
        return num1 + num2;
    }

    static int subtract(int num1, int num2) {
        return num1 - num2;
    }

    static int multiply(int num1, int num2) {
        return num1 * num2;
    }

    static int divide(int num1, int num2) {
        return num1 / num2;
    }
}
